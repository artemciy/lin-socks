#![no_std]

#![feature (non_ascii_idents)]
#![allow (unknown_lints, uncommon_codepoints)]

#![feature (alloc_error_handler)]
#![feature (panic_info_message)]

#![cfg_attr (test, allow (dead_code))]

//#[macro_use]
extern crate alloc;

use core::alloc::{GlobalAlloc, Layout};
use core::ffi::c_void;
use core::fmt::{self, Write as FmtWrite};
use core::ptr::null_mut;
use core::sync::atomic::{AtomicUsize, Ordering};
use inlinable_string::{InlinableString, StringExt};

extern "C" {
  // --- C helpers ---

  static C_GFP_KERNEL: u32;

  /// * `loc` - null-terminated location string
  /// * `len` - length of `line`
  /// * `line` - payload to be logged at `KERN_INFO`
  fn c_pid_loc_info (loc: *const u8, len: u32, line: *const u8);

  fn c_num_online_cpus() -> i32;

  /// `threadfn` - a function to run as a kernel thread
  /// `data` - passed to `threadfn`
  /// `cpu` - if >= 0 then `kthread_bind` is used to set the affinity
  /// `name` - the name of the kernel thread
  fn c_kthread_run (
    threadfn: extern "C" fn (data: *mut c_void) -> i32,
    data: *mut c_void, cpu: i8, name: *const u8) -> *mut c_void;

  fn c_signal_pending() -> bool;

  // --- kernel functions ---

  /// Sets `kthread_should_stop` for `task` to return `true`, wakes it, and waits for it to exit  
  /// Returns the result of `threadfn`, or `-EINTR` if `wake_up_process` was never called
  fn kthread_stop (task: *mut c_void) -> i32;

  fn kthread_should_stop() -> bool;
  fn msleep (msecs: u32);
  fn msleep_interruptible (msecs: u32) -> u64;
  fn dump_stack();
  fn krealloc (p: *mut c_void, new_size: u32, flags: u32) -> *mut c_void;
  fn kfree (p: *mut c_void);}

/// path -> filename
///
/// Returns the unchanged `path` if there is a character encoding error or something
pub fn filename<'a> (path: &'a str) -> &'a str {
  let name = match path.rfind (|ch| ch == '/' || ch == '\\') {
    Some (ofs) => &path[ofs+1..],
    None => path};

  if name.ends_with (".rs") {&name[0 .. name.len() - 3]} else {name}}

/// Returns on error, converting the `Err` value to `String` and prepending the current location
#[macro_export] macro_rules! try_s {
  ($e: expr) => {match $e {
    Ok (ok) => ok,
    Err (err) => {return Err (format! ("{}:{}] {}", $crate::filename (file!()), line!(), err));}}}}

pub fn _log (file: &'static str, line: u32, is: InlinableString, rc: Result<(), fmt::Error>) {
  if let Err (_err) = rc {
    unsafe {c_pid_loc_info (b"log!\0".as_ptr(), 7, b"!write!\0".as_ptr())};
    return}

  let mut loc = InlinableString::new();
  let rc = write! (&mut loc, "{}:{}\0", filename (file), line);
  if let Err (_err) = rc {
    unsafe {c_pid_loc_info (b"_log\0".as_ptr(), 7, b"!write!\0".as_ptr())};
    return}

  unsafe {c_pid_loc_info (loc.as_ptr(), is.len() as u32, is.as_ptr())}}

#[macro_export]
macro_rules! log {
  ($($args: tt)+) => {{
    // NB: We do not want to use too much stack in the kernel, cf. https://lwn.net/Articles/600644/
    // Small String Optimization of InlinableString seems reasonable, but larger SmallVec stacks not so much
    let mut is = InlinableString::new();
    let rc = write! (&mut is, $($args)+);
    $crate::_log (file!(), line!(), is, rc)}}}

extern "C"
fn threadfn (data: *mut c_void) -> i32 {
  log! ("threadfn; data: {:?}", data);
  loop {
    unsafe {msleep_interruptible (3141)};
    if unsafe {kthread_should_stop()} {break}
    if unsafe {c_signal_pending()} {break}}
  0}

static TASK: AtomicUsize = AtomicUsize::new (0);

/// Linux kernel module initialization. Invoked from inside the `insmod` process
#[no_mangle] pub extern "C"
fn r_start() {
  let cpus = unsafe {c_num_online_cpus()};
  let cpu = cpus.min (8) as i8 - 1;
  log! ("r_start] cpus: {}; cpu: {}", cpus, cpu);
  if TASK.compare_and_swap (0, 1, Ordering::Relaxed) == 0 {
    let task = unsafe {c_kthread_run (threadfn, null_mut(), cpu, b"lin-socks\0".as_ptr())} as usize;
    TASK.store (task, Ordering::Relaxed)}}

/// Linux kernel module destructor. Triggered by `rmmod`
#[no_mangle] pub extern "C"
fn r_exit() {
  let task = TASK.load (Ordering::Relaxed);
  if task != 0 && TASK.compare_and_swap (task, 0, Ordering::Relaxed) == task {
    log! ("Stopping task…");
    let rc = unsafe {kthread_stop (task as *mut c_void)};
    log! ("threadfn rc: {}", rc)}}

// --- panic handler ---

#[cfg_attr (not(test), panic_handler)]
fn panic_handler (info: &core::panic::PanicInfo) -> ! {
  let mut it = 0;
  loop {
    // We do not want to crash the kernel and OS,
    // as it might be harder after a crash to learn of the issue and/or to further troubleshoot it
    // So instead of crashing - we sleep
    // cf. https://www.kernel.org/doc/Documentation/timers/timers-howto.txt

    log! ("panic! iteration {}", it);
    unsafe {dump_stack()};

    let mut is = InlinableString::new();
    if let Some (loc) = info.location() {let _ = write! (&mut is, "{} ", loc);}
    if let Some (msg) = info.message() {let _ = write! (&mut is, "{}", msg);}
    if !is.is_empty() {unsafe {c_pid_loc_info (b"panic_handler\0".as_ptr(), is.len() as u32, is.as_ptr())}}

    unsafe {msleep_interruptible (31415)};  // Wake up early on a signal
    unsafe {msleep (3141)};  // Spam not the log even if spammed by signals on reboot
    it += 1}}

// --- heap allocation ---
// https://rust-embedded.github.io/book/collections/index.html
// https://github.com/fishinabarrel/linux-kernel-module-rust/blob/6923f8b/src/allocator.rs

struct MyAllocator;

unsafe impl GlobalAlloc for MyAllocator {
  unsafe fn alloc (&self, layout: Layout) -> *mut u8 {
    if layout.size() > u32::max_value() as usize {panic! ("size > u32")}
    let rc = krealloc (null_mut(), layout.size() as u32, C_GFP_KERNEL);
    // Linux logs a WARNING and returns 0 if we're asking too much
    if rc == null_mut() {on_oom (layout)}
    rc as *mut u8}

  unsafe fn dealloc (&self, ptr: *mut u8, _layout: Layout) {
    kfree (ptr as *mut c_void)}}

#[cfg_attr (not (test), global_allocator)]
static A: MyAllocator = MyAllocator;

#[cfg_attr (not (test), alloc_error_handler)]
fn on_oom (layout: Layout) -> ! {
  let mut it = 0;
  loop {
    log! ("OOM on {}, iteration {}", layout.size(), it);
    unsafe {dump_stack()};
    unsafe {msleep_interruptible (31415)};
    unsafe {msleep (3141)};
    it += 1}}
