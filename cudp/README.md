# cUDP

Communicate with UDP servers

## UDP client

Connect to a given port, send a given message and echo the reply

TBD: Send a command line message  
TBD: Send a piped stdin message  
TBD: Example using cUDP with `strings` and `grep`  
TBD: Send messages from an “in” folder and write replies to an “out” folder  

## UDP server

Run a UDP server on a given port in order to see what goes there

cf. https://gist.github.com/Manouchehri/67b53ecdc767919dddf3ec4ea8098b20  
cf. https://github.com/christian-smith/rust-udp-server

TBD: Manual UDP repllies, from stdin and/or command line  
TBD: Piping incoming UDP packets to a command and the output of the command back to the UDP

## UDP tracing

TBD: Discover and list local microservices with tracing support  
TBD: Start a tracing session, where a given microservice is asked to forward a subset of its UDP packets and/or logs to cUDP  
TBD: Remote tracing
