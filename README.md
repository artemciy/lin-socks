From Linux kernel, maintain a WebSocket connection to a number of servers and process their streams.

In order not to waste resources on context switching delivering the streams to user space, when all we want is a bit of slicing and dicing.

# prerequisites

On Debian it needs the linux-headers package which corresponds to the currently running version of the Linux kernel
