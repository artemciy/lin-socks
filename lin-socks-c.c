#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/sched.h>  // current
#include <linux/kthread.h>

#include <linux/version.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION (4, 11, 0)
# include <linux/sched/signal.h>  // signal_pending
#endif

MODULE_LICENSE ("GPL");
MODULE_AUTHOR ("Homer S.");
MODULE_DESCRIPTION ("hmmmm ...");
MODULE_VERSION ("1.0");

// --- helpers for Rust code ---

uint32_t C_GFP_KERNEL = GFP_KERNEL;

void c_pid_loc_info (char const * loc, uint32_t len, char const * line) {
  // https://en.wikipedia.org/wiki/Printk
  // https://www.spinics.net/lists/kernel/msg3583729.html printk of non NULL terminated strings
  printk (KERN_INFO "pid %d] %s] %.*s\n", current->pid, loc, len, line);}

// The `lscpu` command can be used to check the number of CPUs and NUMA nodes

void* c_kthread_run (int32_t (*threadfn) (void* data), void* data, int8_t cpu, char const * name) {
  struct task_struct * task = kthread_create (threadfn, data, "%s", name);
  if (IS_ERR (task)) return 0;
  if (cpu >= 0) kthread_bind (task, cpu);
  wake_up_process (task);
  return (void*) task;}

bool c_signal_pending (void) {return signal_pending (current);}

int32_t c_num_online_cpus (void) {return num_online_cpus();}

// --- Rust functions ---

void r_start (void);
void r_exit (void);

// --- mod ---

static int __init
start (void) {
  r_start();
  return 0;}

static void __exit
end (void) {
  r_exit();}

module_init (start);
module_exit (end);
