# https://www.kernel.org/doc/Documentation/kbuild/makefiles.txt

obj-m += lin-socks.o

# NB “you can't have a C file that has the same name as the module when trying to build a multiple file module”
# https://stackoverflow.com/a/39268319/257568

lin-socks-objs := lin-socks-c.o liblin_socks.a

#LDFLAGS += --verbose

KVERSION = $(shell uname -r)

all:
	make -C /lib/modules/$(KVERSION)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(KVERSION)/build M=$(PWD) clean
