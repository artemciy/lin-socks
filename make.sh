#!/bin/dash

set -ex

RUSTFLAGS=-Ctarget-cpu=native cargo build --release -Z build-std=core,alloc --target=x86_64-linux-kernel
ln -f target/x86_64-linux-kernel/release/liblin_socks.a
#ld -r --whole-archive -o lin_socks_rs.o liblin_socks.a

make

rmmod lin_socks ||:

sleep 0.1

insmod lin-socks.ko

sleep 0.1

lsmod | grep lin_sock
