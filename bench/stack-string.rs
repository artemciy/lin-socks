// (cd bench && RUSTFLAGS=-Ctarget-cpu=native nice --19 cargo bench)

/* 2020-11, seeing different results on different hardware:

old i7
write_string            time:   [142.63 ns 143.45 ns 144.32 ns]
write_cursor            time:   [41.238 ns 41.339 ns 41.456 ns]
write_cursor_uninit     time:   [37.525 ns 37.588 ns 37.661 ns]
write_smallvec          time:   [62.710 ns 62.767 ns 62.843 ns]
write_inlinable_string  time:   [54.599 ns 54.631 ns 54.668 ns]

VPS, QEMU Virtual CPU version 2.5+
write_string            time:   [108.38 ns 109.77 ns 111.61 ns]
write_cursor            time:   [47.222 ns 47.999 ns 48.945 ns]
write_cursor_uninit     time:   [44.930 ns 45.296 ns 45.691 ns]
write_smallvec          time:   [56.464 ns 56.991 ns 57.739 ns]
write_inlinable_string  time:   [68.181 ns 68.313 ns 68.510 ns]

The size of the buffer (32 vs 256) gives more of a hit on the VPS

Should add https://crates.io/crates/criterion-perf-events too */

#![no_std]

#[macro_use]
extern crate criterion;

extern crate alloc;

use alloc::string::String;
use bare_io::{Cursor, Write};
use core::fmt::Write as FmtWrite;
use core::mem::MaybeUninit;
use criterion::{black_box, Criterion};
use inlinable_string::{InlinableString, StringExt};
use smallvec::{Array, SmallVec};

struct WriteSmallVec<A: Array<Item=u8>> (SmallVec<A>);
impl<A: Array<Item=u8>> Write for WriteSmallVec<A> {
  fn write (&mut self, buf: &[u8]) -> bare_io::Result<usize> {
    //for ch in buf {self.0.push (*ch)}
    self.0.extend_from_slice (buf);
    Ok (buf.len())}
  fn flush (&mut self) -> bare_io::Result<()> {Ok(())}}

fn write_string (cri: &mut Criterion) {
  cri.bench_function ("write_string", |b| b.iter (|| {
    let mut s = String::new();
    write! (&mut s, "hello world, {}", 123) .expect ("!write");
    black_box (s);}));}

fn write_cursor (cri: &mut Criterion) {
  cri.bench_function ("write_cursor", |b| b.iter (|| {
    let mut buf = [0 as u8; 32];
    let mut cur = Cursor::new (&mut buf[..]);
    write! (&mut cur, "hello world, {}", 123) .expect ("!write");
    black_box (buf);}));

  cri.bench_function ("write_cursor_uninit", |b| b.iter (|| {
    let mut buf: [u8; 32] = unsafe {MaybeUninit::uninit().assume_init()};
    let mut cur = Cursor::new (&mut buf[..]);
    write! (&mut cur, "hello world, {}", 123) .expect ("!write");
    black_box (buf);}));}

fn write_smallvec (cri: &mut Criterion) {
  cri.bench_function ("write_smallvec", |b| b.iter (|| {
    let mut sv = WriteSmallVec (SmallVec::<[u8; 32]>::new());
    write! (&mut sv, "hello world, {}", 123) .expect ("!write");
    black_box (sv.0);}));}

fn write_inlinable_string (cri: &mut Criterion) {
  cri.bench_function ("write_inlinable_string", |b| b.iter (|| {
    let mut is = InlinableString::new();
    write! (&mut is, "hello world, {}", 123) .expect ("!write");
    black_box (is);}));}

criterion_group! (benches, write_string, write_cursor, write_smallvec, write_inlinable_string);
criterion_main! (benches);
